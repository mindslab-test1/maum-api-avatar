package ai.maum.avatar

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AvatarApplication

fun main(args: Array<String>) {
    runApplication<AvatarApplication>(*args)
}
