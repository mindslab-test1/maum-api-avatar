package ai.maum.avatar.bean.rdo

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import java.time.Instant

// Request Data Object
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
data class RequestInfo(
        var remoteHost: String? = null,
        var requestMethod: String? = null,
        var requestPayloadSize: Long? = null,
        var requestTime: Instant? = null,
        var requestContentType: String? = null,
        var requestPathAbsolute: String? = null,
        var responseStatusCode: Long? = null,
        var responseTime: Instant? = null
)