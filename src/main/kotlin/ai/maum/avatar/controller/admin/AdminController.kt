package ai.maum.avatar.controller.admin

import ai.maum.avatar.controller.admin.example_api.ExampleApi
import ai.maum.avatar.controller.admin.example_api.ExampleApiRequestDto
import ai.maum.avatar.extension.controller.ExtendedController
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class AdminController(
) : ExtendedController {
    @Transactional
    @PostMapping("/admin/exampleapi")
    fun get(@RequestBody @Valid requestDto: ExampleApiRequestDto) = handler {
        w controller this@AdminController
        w dto requestDto
        w handler ExampleApi(this@AdminController, requestDto)
    }
}