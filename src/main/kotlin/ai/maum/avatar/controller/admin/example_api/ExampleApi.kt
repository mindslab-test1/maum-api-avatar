package ai.maum.avatar.controller.admin.example_api

import ai.maum.avatar.controller.admin.AdminController
import ai.maum.avatar.extension.controller.HandlerType
import ai.maum.avatar.extension.controller.ResponseType

class ExampleApi(controller: AdminController, dto: ExampleApiRequestDto)
    : HandlerType<AdminController, ExampleApiRequestDto>(controller, dto) {
    override fun invoke(): ResponseType {
        TODO("Not yet implemented")
    }
}