package ai.maum.avatar.controller.admin.example_api

import kotlinx.serialization.Serializable

@Serializable
data class ExampleApiRequestDto(
        var tbd: String
)