package ai.maum.avatar.controller.admin.example_api

import kotlinx.serialization.Serializable

@Serializable
data class ExampleApiResponseDto(
        var tbd: String
)