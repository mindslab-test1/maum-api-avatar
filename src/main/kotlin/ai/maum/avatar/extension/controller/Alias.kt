package ai.maum.avatar.extension.controller

import org.springframework.http.ResponseEntity

typealias ResponseType = ResponseEntity<Any>
typealias HandlerMethod = () -> ResponseType

abstract class HandlerType<Controller, Dto>(val controller: Controller, val dto: Dto) : HandlerMethod

@DslMarker
annotation class HandlerMarker

class Alias