package ai.maum.avatar.extension.controller

interface ExtendedController {
    fun handler(description: HandlerProcess.() -> Unit): ResponseType {
        val handlerProcess = HandlerProcess()
        description(handlerProcess)
        handlerProcess.handle()

        return handlerProcess.response!!
    }
}