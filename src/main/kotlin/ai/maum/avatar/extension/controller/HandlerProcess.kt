package ai.maum.avatar.extension.controller

@HandlerMarker
class HandlerProcess {
    var w = Statement.RUN
    var a = Statement.TRY

    var response: ResponseType? = null
    var controller: ExtendedController? = null
    var dto: Any? = null
    var handler: HandlerMethod? = null

    infix fun <T : ExtendedController> Statement.controller(controller: T) = wrap {
        this@HandlerProcess.controller = controller
    }

    infix fun <T> Statement.dto(dto: T) = wrap {
        this@HandlerProcess.dto = dto
    }

    infix fun <T : HandlerMethod> Statement.handler(handler: T) = wrap {
        this@HandlerProcess.handler = handler
    }

    fun handle() {
        response = handler!!()
    }

    enum class Statement {
        RUN,
        TRY;

        fun wrap(block: () -> Unit): Statement {
            when (this) {
                RUN -> block()
                TRY -> {
                    try {
                        block()
                    } catch (e: Exception) {
                        // Swallow exceptions
                    }
                }
            }
            return this
        }
    }
}
