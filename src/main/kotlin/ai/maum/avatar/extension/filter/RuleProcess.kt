package ai.maum.avatar.extension.filter

class RuleProcess {
    private val skipList: MutableList<String> = mutableListOf()
    private val allowList: MutableList<String> = mutableListOf()
    private val skipHeadingsList: MutableList<String> = mutableListOf()
    private val allowHeadingsList: MutableList<String> = mutableListOf()

    fun getSkipList(): List<String> = skipList
    fun getAllowList(): List<String> = allowList
    fun getSkipHeadingsList(): List<String> = skipHeadingsList
    fun getAllowHeadingsList(): List<String> = allowHeadingsList

    val skip = RuleProcessPolicy.SKIP
    val allow = RuleProcessPolicy.ALLOW

    infix fun RuleProcessPolicy.equals(value: String): RuleProcessPolicy {
        when (this) {
            RuleProcessPolicy.SKIP -> skipEquals(value)
            RuleProcessPolicy.ALLOW -> allowEquals(value)
        }
        return this
    }

    infix fun RuleProcessPolicy.startsWith(value: String): RuleProcessPolicy {
        when (this) {
            RuleProcessPolicy.SKIP -> skipStartsWith(value)
            RuleProcessPolicy.ALLOW -> allowStartsWith(value)
        }
        return this
    }

    private fun skipEquals(value: String) = skipList.add(value)
    private fun skipStartsWith(value: String) = skipHeadingsList.add(value)

    private fun allowEquals(value: String) = allowList.add(value)
    private fun allowStartsWith(value: String) = allowHeadingsList.add(value)

    enum class RuleProcessPolicy {
        SKIP,
        ALLOW
    }
}