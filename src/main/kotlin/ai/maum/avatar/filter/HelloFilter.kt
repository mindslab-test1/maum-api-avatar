package ai.maum.avatar.filter

import ai.maum.avatar.bean.rdo.RequestInfo
import ai.maum.avatar.extension.filter.FilterUrlWrapper
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(0)
@Component
class HelloFilter(userInfoFactory: ObjectFactory<RequestInfo>) : FilterUrlWrapper(HelloFilterImpl(userInfoFactory)) {
    init {
        skipRules {
            skip equals "/test1" equals "/test2"
            skip equals "/test3"

            skip startsWith "/admin/"
        }
    }
}