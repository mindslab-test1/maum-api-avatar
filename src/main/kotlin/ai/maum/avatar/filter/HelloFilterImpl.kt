package ai.maum.avatar.filter

import ai.maum.avatar.bean.rdo.RequestInfo
import ai.maum.avatar.extension.filter.FilterImpl
import org.springframework.beans.factory.ObjectFactory
import java.time.Instant
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class HelloFilterImpl(
        var requestInfoFactory: ObjectFactory<RequestInfo>,
        // Database repository here if needed
) : FilterImpl {
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val requestInfo = requestInfoFactory.`object`

        requestInfo.remoteHost = request.remoteHost
        requestInfo.requestMethod = request.method
        requestInfo.requestPayloadSize = request.contentLengthLong
        requestInfo.requestTime = Instant.now()
        requestInfo.requestContentType = request.contentType
        requestInfo.requestPathAbsolute = request.requestURI

        try {
            chain.doFilter(request, response)
            requestInfo.responseStatusCode = response.status.toLong()
        } catch (e: Exception) {
            requestInfo.responseStatusCode = 500L
            throw e
        } finally {
            requestInfo.responseTime = Instant.now()

            // Save to database here if needed
        }
    }
}