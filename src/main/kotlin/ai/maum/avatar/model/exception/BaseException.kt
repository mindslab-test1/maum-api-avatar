package ai.maum.avatar.model.exception

abstract class BaseException : RuntimeException {
    var codeOffset: Long? = null

    constructor(codeOffset: Long, message: String?, cause: Throwable?) : super(message, cause) {
        this.codeOffset = codeOffset
    }

    constructor(codeOffset: Long, message: String?) : super(message) {
        this.codeOffset = codeOffset
    }

    constructor(codeOffset: Long, cause: Throwable?) : super(cause) {
        this.codeOffset = codeOffset
    }

    constructor(codeOffset: Long) : this(codeOffset, "") {
        this.codeOffset = codeOffset
    }

    open val baseCode = 0L
    fun code() : Long = codeOffset!! + baseCode
}