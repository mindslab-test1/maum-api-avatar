package ai.maum.avatar.model.exception

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class BaseExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(BaseException::class)
    fun handle(exception: BaseException, request: WebRequest): ResponseEntity<Any> =
            handleExceptionInternal(
                    exception,
                    BaseExceptionResponse(
                            exception.code(),
                            exception::class.simpleName ?: "<unnamed>",
                            exception.message ?: ""
                    ),
                    HttpHeaders(),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    request
            )
}

class BaseExceptionResponse(
        var code: Long,
        var name: String,
        var message: String
)