package ai.maum.avatar.model.exception

class SampleException : BaseException {
    constructor(codeOffset: Long, message: String?, cause: Throwable?) : super(codeOffset, message, cause)
    constructor(codeOffset: Long, message: String?) : super(codeOffset, message)
    constructor(codeOffset: Long, cause: Throwable?) : super(codeOffset, cause)
    constructor(codeOffset: Long) : super(codeOffset)

    override val baseCode = 10000L
}